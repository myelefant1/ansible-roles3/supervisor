## Supervisor

This role:

  * installs supervisor
  * configures minfds the minimum number of file descriptors
  * configures his own systemd configuration file

## Role parameters

| name                             | value       | optionnal | default_value | description                                       |
| ---------------------------------|-------------|-----------|---------------|---------------------------------------------------|
| supervisor_systemd_file          | string      | Yes       | N/A           | override systemd configurationf file              |
| supervisor_service_file          | string      | Yes       | N/A           | deploy a configuration file for given service     |
| supervisor_ini_params            | list        | Yes       | []            | configuring `/etc/supervisor/supervisord.conf`    |
| supervisor_default_permission    | integer     | Yes       | 0644          | permissions of configuration files                |
| supervisor_systemd_service_state | string      | Yes       | started       | starte of service at deployment                   |

* *supervisor_ini_params* :

Values for file /etc/supervisord/supervisor.conf, list of values with given keys: section, option and value.

Exemple:

```yaml
supervisor_ini_params:
  - { section: inet_http_server, option: port, value: 127.0.0.1:19200 }
  - { section: supervisord, option: minfds, value: 1024 }
```

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/ansible-roles3/supervisor.git
  scm: git
  version: v1.1
```

### Sample playbook


#### Basic usage

Installs and enables supervisord service

```yaml
---
- hosts: all
  roles:
      - role: supervisor
```

#### Another usage

Installs and enables supervisord service, specifying `rlimit_nofile`

```yaml
---
- hosts: all
  roles:
      - role: supervisor
        supervisor_service_file: tests/files/http_server.conf
        supervisor_ini_params:
          - { section: inet_http_server, option: port, value: 127.0.0.1:19200 }
          - { section: supervisord, option: minfds, value: 2048 }
        supervisor_systemd_file: my_systemdconfig_for_supervisor.service
```

## Tests

[tests/tests_supervisor](tests/tests_supervisor)
